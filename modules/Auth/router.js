const express = require('express');
const router = express.Router();
const model = require('./model');
const controller = require('./controller');

// router.post('/signup',controller.register);
router.post('/signup',controller.register);

module.exports = router;
