const mongoose = require('mongoose');
const User = mongoose.model('User')
saveUser = (data)=>{
   const user  = new User(data);
   return user.save();
}

module.exports = {
saveUser
}