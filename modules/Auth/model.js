// const mongoose = require("mongoose");
// const md5 = require("md5");

// const Schema = mongoose.Schema;

// const userSchema = new Schema({
//     id: { type: String, required: true },
//     username: { type: String, required: true },
//     password: { type: String, required: true },
//     name: { type: String, required: true },
//     age: { type: Number, required: true, min: 18 },
//     sex: { type: String, required: true, enum: ["male", "female"] },
// }, {
//     timestamps: true,
// });

// userSchema.methods.setPass = function (password) {
//     this.password = md5(password);
// };

// userSchema.methods.checkPass = function (password) {
//     return this.password === md5(password);
// };

// module.exports = mongoose.model("User", userSchema);

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const userSchema = new Schema({
    id: { type: String },
    name: { type: String, required: true },
    email: { type: String, required: true },
    mobile: { type: Number, required: true },
    address: { type: String, required: true },
    city: { type: String, required: true },
    state: { type: String, required: true },
    country: { type: String, required: true },
    pincode: { type: Number, required: true },
}, {
    timestamps: true,
})

module.exports = mongoose.model("User", userSchema);