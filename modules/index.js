const usersRouter = require( "./Auth/router" );
// const articlesRouter = require( "./articles/router" );
// const sessionRouter = require( "./session/router" );
// const validateToken = require( "../middlewares/validateToken" );

module.exports = ( app ) => {
    // app.use( "/session", sessionRouter );
    app.use( "/auth", usersRouter );
    // app.use( "/articles", validateToken, articlesRouter );
};