const express = require('express');
const app = express();
// const bodyParser = require( "body-parser" );
const cors = require('cors');
const config = require( "./config" );
const port = process.env.PORT || config.port;
const ENV = process.env.NODE_ENV || config.env;

// const mongoose = require( "./config/mongoose" )( app );
app.use(express.json());
app.use(express.urlencoded({extended:true}))
app.use(cors());


require( "./config/mongoose" )( app );
require( "./modules/index" )( app );
 


app.get('/first', (req,res)=>{ 
   res.send("yoooo....")
 })

app.listen(port, res=>{
   console.log('Port running on 4000');
  })