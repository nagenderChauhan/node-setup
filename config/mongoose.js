const config = require('./index');
const mongoose = require('mongoose');

module.exports = function (app) {
    mongoose.connect(config.mongoUrl, { useUnifiedTopology: true, useNewUrlParser: true }).then(res => {
        console.log(`db connection on port 27017`);
    }

    ).catch(err => {
        console.log(err)
    });
    mongoose.Promise = global.Promise;

    if (app) {
        app.set("mongoose", mongoose)
    }
}

function cleanUp() {
    mongoose.connection.close(function () {
        process.exit(0);
    })
}