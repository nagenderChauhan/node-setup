// module.exports = {
//     host: "127.0.0.1",
//     port: 1234, // change with development port
//     mongoUrl: "mongodb://localhost:27017/projectDbName", // replace "projectDbName" with a proper db name
//     logLevel: "debug", // can be chenged to error, warning, info, verbose or silly
//     secret: "superSuperSecret",
// };

module.exports = {
    port : 4000,
    mongoUrl : "mongodb://localhost:27017/farm"
}